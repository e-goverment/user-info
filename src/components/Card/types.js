const CARD_TYPES = {
  physical: 1,
  legal: 2,
  car: 3,
  home: 4,
};

export default CARD_TYPES;
