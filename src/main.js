import Vue from 'vue';
import VModal from 'vue-js-modal';
import Loading from 'vue-loading-overlay';
import App from './App.vue';
import router from './router';
import store from './store';

import 'vue-loading-overlay/dist/vue-loading.css';

Vue.use(VModal);

const SECRET_KEY = 'asdfrewqtyubgtrenb546oki==';
const BASE_URL = 'http://172.16.60.8:8083/api';

Vue.use(Loading, {
  isFullPage: false,
});

Vue.config.productionTip = false;
Vue.prototype.$BASE_URL = BASE_URL;
Vue.prototype.$SECRET_KEY = SECRET_KEY;

Vue.prototype.$getItems = async (url, params) => {
  const data = {
    queryId: '123',
    secret: SECRET_KEY,
  };

  // eslint-disable-next-line no-restricted-syntax
  for (const key in params) {
    if (Object.hasOwnProperty.call(params, key)) {
      data[key] = params[key];
    }
  }

  try {
    const response = await fetch(`${BASE_URL}/${url}`, {
      headers: new Headers({ 'content-type': 'application/json' }),
      method: 'post',
      body: JSON.stringify(data),
    });
    const json = await response.json();

    return json;
  } catch (error) {
    console.log('error in get items method');
    console.log(error);
    // eslint-disable-next-line no-throw-literal
    throw ('error in get items method');
    // eslint-disable-next-line no-unreachable
    return error;
  }
};

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
