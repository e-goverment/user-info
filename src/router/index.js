import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home/index.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home,
    children: [
      {
        path: 'general',
        name: 'GeneralInfo',
        component: () => import(/* webpackChunkName: "general" */ '../views/Home/General/index.vue'),
      },
      {
        path: 'applications',
        name: 'Applications',
        component: () => import(/* webpackChunkName: "other" */ '../views/Home/Other.vue'),
      },
      {
        path: 'info',
        name: 'Info',
        component: () => import(/* webpackChunkName: "other" */ '../views/Home/Other.vue'),
      },
      {
        path: 'payment-history',
        name: 'PaymentHistory',
        component: () => import(/* webpackChunkName: "other" */ '../views/Home/Other.vue'),
      },
      {
        path: 'gas',
        name: 'Gas',
        component: () => import(/* webpackChunkName: "other" */ '../views/Home/Gas/index.vue'),
      },
      {
        path: 'energy',
        name: 'Energy',
        component: () => import(/* webpackChunkName: "other" */ '../views/Home/Energy/index.vue'),
      },
      {
        path: 'car',
        name: 'Car',
        component: () => import(/* webpackChunkName: "other" */ '../views/Home/Car/index.vue'),
      },
      {
        path: 'legal',
        name: 'Legal',
        component: () => import(/* webpackChunkName: "other" */ '../views/Home/Legal/index.vue'),
      },
      {
        path: 'hause',
        name: 'Hause',
        component: () => import(/* webpackChunkName: "other" */ '../views/Home/Hause/index.vue'),
      },
      {
        path: '/',
        redirect: 'general',
      },
    ],
  },
  {
    path: '/',
    name: 'Enter',
    component: () => import(/* webpackChunkName: "about" */ '../views/Enter.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

export default router;
