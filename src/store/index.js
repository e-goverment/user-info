import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isLegal: false,
    cadastrs: [],
    legalData: null,
    type: 'physical',
    legalCadastrs: null,
  },
  mutations: {
    SET_TYPE(state, value) {
      state.type = value;
    },
    SET_IS_LEGAL(state, value) {
      state.isLegal = value;
    },
    SET_CADASTRS(state, value) {
      state.cadastrs = value;
    },
    SET_LEGAL_DATA(state, value) {
      state.legalData = value;
    },
  },
  modules: {
  },
});
